﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums4
{
    class Program
    {
        static void Main(string[] args)
        {
            object object1 = Enum.Parse(typeof(EnumType), "Ten"); // Boxing
            EnumType enu = (EnumType)object1; // UnBoxing

            bool flag = Enum.IsDefined(typeof(EnumType), "One");
            if (flag)
                Console.WriteLine("There is constant One");
            else
                Console.WriteLine("There is not ");
        }

        enum EnumType
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Ten = 10,
        }
    }
}
