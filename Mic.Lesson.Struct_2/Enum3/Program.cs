﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enum3
{
    class Program
    {
        static void Main(string[] args)
        { 
            EnumType var10 = EnumType.Ten;
            Console.WriteLine("X 16 - {0}", Enum.Format(typeof(EnumType), EnumType.Ten, "x"));
            Console.WriteLine("X 10 - {0}", Enum.Format(typeof(EnumType), var10, "D"));
            Console.WriteLine("String - {0}", Enum.Format(typeof(EnumType), (byte)10, "G"));
        }

        enum EnumType
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Ten = 10
        }
    }
}
