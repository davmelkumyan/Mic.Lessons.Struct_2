﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums2
{
    class Program
    {
        static void Main(string[] args)
        {
            EnumType var1 = EnumType.One;
            Type type1 = var1.GetType();
            Console.Write($"Enumeration {type1.Name} is usingt type ");
            Type type2 = Enum.GetUnderlyingType(type1);
            Console.WriteLine($"{type2.Name}");

            Console.WriteLine($"Enumeration {type1.Name} is usingt type" + Enum.GetUnderlyingType(typeof(EnumType)));
        }

        enum EnumType
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Ten = 10
        }
    }
}
