﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CovariantStruct
{
    class Program
    {
        static void Main(string[] args)
        {
            MyStruct[] array = { new MyStruct(), new MyStruct(), new MyStruct() };

            // IInterface[] covariant = array; // Ковариянтность и КонтрВариантность не работают со Структурами

            int[] arr = { 1, 2, 3, 4, 5 };
            // object[] objs = arr; // Ковариянтность и КонтрВариантность не работают со Структурами

            uint[] arrr = new uint[5];
            Console.WriteLine("{0}, {1}", arrr is uint[], arrr is int[]);
            object obj = arrr;
            Console.WriteLine("{0} {1}", obj is uint[], obj is int[]);
        }

        interface IInterface
        {
            void Method();
        }

        struct MyStruct : IInterface
        {
            public void Method()
            {
                Console.WriteLine("Method");
            }
        }
    }
}
