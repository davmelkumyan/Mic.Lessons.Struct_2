﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateStructure
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime now = DateTime.Now;
            DateTime christmasDate = new DateTime(2018, 12, 31, 23, 59, 59);
            TimeSpan ts = christmasDate - now;
            Console.WriteLine($"Days Left - {ts}");

            DateTime dt = new DateTime(2019, 1, 1, 23, 59, 59); // Структура Времени
            DateTime dt2 = DateTime.Now;
            TimeSpan ts2 = dt - dt2; // Интервал времени
            Console.WriteLine(dt.TimeOfDay);

            Console.WriteLine($"now:D - {now:D}");
            Console.WriteLine($"now:d - {now:d}");
            Console.WriteLine($"now:F - {now:F}");
            Console.WriteLine($"now:G - {now:G}");
            Console.WriteLine($"now:g - {now:g}");
            Console.WriteLine($"now:M - {now:M}");
            Console.WriteLine($"now:Y - {now:Y}");
            Console.WriteLine($"now:T - {now:T}");
            Console.WriteLine($"now:t - {now:t}");
        }
    }
}
