﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(EnumType.One); // One
            Console.WriteLine((byte)EnumType.Zero); // 0
        }

        enum EnumType : byte
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Ten = 10
        }
    }
}
