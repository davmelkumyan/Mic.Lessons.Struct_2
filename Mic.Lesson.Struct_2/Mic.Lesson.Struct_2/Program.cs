﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Struct_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Boxing1 ////////////////////////////
            int a = 10;
            //Boxing
            Object obj = a;
            //UnBoxing
            int b = (int)obj;



            // Boxing 2 /////////////////////////////////////
            MyStruct2 variable;
            variable.Method();

            //Boxing 
            ValueType vt = variable;
            //Incapsulation in vt for variable
            //vt.Method();

            //UnBoxing
            MyStruct2 variable2 = (MyStruct2)vt;
            variable2.Method(); // is working


            // Boxing3/////////////////////////////
            MyStruct value;
            value.Method();
            //Boxing to interface base type
            IInterface someInterface = value;
            someInterface.Method();

            //UnBoxing
            MyStruct value2 = (MyStruct)someInterface;
            value2.Method();

        }
    }
}
